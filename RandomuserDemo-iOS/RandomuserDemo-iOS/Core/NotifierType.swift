//
//  NotifierDelegate.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation
import RxSwift

protocol NotifierType {
    // NOTE: Can be modified to args, or whatever is applicable
    func notify<DefaultArgument,AdditionalArgument>(arg1 :DefaultArgument, arg2 :AdditionalArgument)
    func notify<DefaultArgument>(arg1: DefaultArgument)
    func notify()
}

// MARK: Make Notifiers optional
extension NotifierType {
    func notify<DefaultArgument,AdditionalArgument>(arg1 :DefaultArgument, arg2 :AdditionalArgument){}
    func notify<DefaultArgument>(arg1: DefaultArgument){}
    func notify(){}
}

