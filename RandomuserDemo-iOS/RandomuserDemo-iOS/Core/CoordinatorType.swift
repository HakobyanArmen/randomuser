//
//  CoordinatorType.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation
import RxSwift

protocol CoordinatorDelegate: class {
    func coordinatorFinishedFlow(coordinator: CoordinatorType)
    func coordinatorExecuteStep(step: CoordinatorStepType)
}

/// Marker protocol for normal steps
protocol CoordinatorStepType { }

/// Marker protocol for observable steps
protocol ObservableCoordinatorStepType {
    associatedtype Result
}

protocol CoordinatorType: HasDisposeBag {
    
    /// Array of child coordinators that were initiated by this coordinator
    var childCoordinators: [CoordinatorType] { get set }
    
    /// Delegate to notify parent that coordinator finished
    var delegate: CoordinatorDelegate? { get set }
    
    /// Executes a coordination step on a specific child coordinator
    func execute(step: CoordinatorStepType, onChild coordinator: CoordinatorType) -> Bool
    
    /// Passes the step to all the child coordinators
    func cascade(step: CoordinatorStepType) -> Bool
    func cascade<Step: ObservableCoordinatorStepType>(step: Step) -> Observable<Step.Result?>
    
    /// Adds a coordinator to the child
    func add(childCoordinator coordinator: CoordinatorType)
    
    func remove(childCoordinator coordinator: CoordinatorType)
    
    /// Tries to handle a navigation step, if it can handle it, returns true. False otherwise.
    @discardableResult
    func execute(step: CoordinatorStepType) -> Bool
    func execute<Step: ObservableCoordinatorStepType>(step: Step) -> Observable<Step.Result?>
    
    // MARK: Notifier Support
    // Passed Notifier Delegate so that Classes can talk to each other
    @discardableResult
    func execute(step: CoordinatorStepType, notifier: NotifierType) -> Bool
    func execute<Step: ObservableCoordinatorStepType>(step: Step, notifier: NotifierType) -> Observable<Step.Result?>
    //Notifier: Executes a coordination step on a specific child coordinator
    func execute(step: CoordinatorStepType, onChild coordinator: CoordinatorType, notifier: NotifierType) -> Bool
    //Notifier: Passes the step to all the child coordinators
    func cascade(step: CoordinatorStepType, notifier: NotifierType) -> Bool
    func cascade<Step: ObservableCoordinatorStepType>(step: Step, notifier: NotifierType) -> Observable<Step.Result?>
}

extension CoordinatorType where Self: CoordinatorDelegate {
    func add(childCoordinator coordinator: CoordinatorType) {
        self.childCoordinators.append(coordinator)
        coordinator.delegate = self
    }
    
    func remove(childCoordinator coordinator: CoordinatorType) {
        self.childCoordinators = self.childCoordinators.filter { $0 !== coordinator }
    }
}

// MARK: With Notifier
extension CoordinatorType {
    func cascade(step: CoordinatorStepType, notifier: NotifierType) -> Bool {
        let childExecuted = childCoordinators.map { child -> Bool? in
            return self.execute(step: step, onChild: child, notifier: notifier)
            }
            .filter { $0 != nil }.map { $0! }
            .reduce(true, { $0 && $1 })
        
        return childExecuted
    }
    
    func execute(step: CoordinatorStepType, onChild coordinator: CoordinatorType, notifier: NotifierType) -> Bool {
        return coordinator.execute(step: step, notifier: notifier)
    }
    
    func execute<Step, Result>(step: Step, notifier: NotifierType) -> Observable<Result?> where Step: ObservableCoordinatorStepType, Step.Result == Result {
        return self.cascade(step: step, notifier: notifier)
    }
    
    func execute<Step, Result>(step: Step, onChild coordinator: CoordinatorType, notifier: NotifierType) -> Observable<Result?> where Step: ObservableCoordinatorStepType, Step.Result == Result {
        return coordinator.execute(step: step, notifier: notifier)
    }
    
    func cascade<Step, Result>(step: Step, notifier: NotifierType) -> Observable<Result?> where Step: ObservableCoordinatorStepType, Step.Result == Result {
        let childExecuted = childCoordinators.map { child -> Observable<Result?> in
            return self.execute(step: step, onChild: child, notifier: notifier)
        }
        return Observable.merge(childExecuted)
    }
}

extension CoordinatorType {
    func execute<Step, Result>(step: Step) -> Observable<Result?> where Step: ObservableCoordinatorStepType, Step.Result == Result {
        return self.cascade(step: step)
    }
    
    var delegate: CoordinatorDelegate? {
        return nil
    }
    
    func execute(step: CoordinatorStepType, onChild coordinator: CoordinatorType) -> Bool {
        return coordinator.execute(step: step)
    }
    
    func execute<Step, Result>(step: Step, onChild coordinator: CoordinatorType) -> Observable<Result?> where Step: ObservableCoordinatorStepType, Step.Result == Result {
        return coordinator.execute(step: step)
    }
    
    func cascade(step: CoordinatorStepType) -> Bool {
        let childExecuted = childCoordinators.map { child -> Bool? in
            return self.execute(step: step, onChild: child)
            }
            .filter { $0 != nil }.map { $0! }
            .reduce(true, { $0 && $1 })
        
        return childExecuted
    }
    
    func cascade<Step, Result>(step: Step) -> Observable<Result?> where Step: ObservableCoordinatorStepType, Step.Result == Result {
        let childExecuted = childCoordinators.map { child -> Observable<Result?> in
            return self.execute(step: step, onChild: child)
        }
        return Observable.merge(childExecuted)
    }
    
}

extension CoordinatorDelegate where Self: CoordinatorType {
    func coordinatorFinishedFlow(coordinator: CoordinatorType) {
        self.remove(childCoordinator: coordinator)
    }
    
    func coordinatorExecuteStep(step: CoordinatorStepType) {
        self.execute(step: step)
    }
}
