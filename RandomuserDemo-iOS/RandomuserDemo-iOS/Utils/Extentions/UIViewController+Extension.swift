//
//  UIViewController+Extension.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    public static var defaultNib: String {
        return self.description().components(separatedBy: ".").dropFirst().joined(separator: ".")
    }
    
    public static var storyboardIdentifier: String {
        return self.description().components(separatedBy: ".").dropFirst().joined(separator: ".")
    }
}

extension UIViewController {
    func topMostViewController() -> UIViewController {
        if let navigation = self as? UINavigationController, let visibleVC = navigation.visibleViewController {
            return visibleVC.topMostViewController()
        }
        
        if let tab = self as? UITabBarController {
            if let selectedTab = tab.selectedViewController {
                return selectedTab.topMostViewController()
            }
            return tab.topMostViewController()
        }
        
        guard let presentedVC = self.presentedViewController else {
            return self
        }
        
        if let navigation = presentedVC as? UINavigationController, let visibleVC = navigation.visibleViewController {
            return visibleVC.topMostViewController()
        }
        
        if let tab = presentedVC as? UITabBarController {
            if let selectedTab = tab.selectedViewController {
                return selectedTab.topMostViewController()
            }
            return tab.topMostViewController()
        }
        return presentedVC.topMostViewController()
    }
}

extension UIViewController {
    func add(_ child: UIViewController) {
        addChild(child)
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    
    func remove() {
        // Just to be safe, we check that this view controller
        // is actually added to a parent before removing it.        
        willMove(toParent: nil)
        view.removeFromSuperview()
        removeFromParent()
    }
}
