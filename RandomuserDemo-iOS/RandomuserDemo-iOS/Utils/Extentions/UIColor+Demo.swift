//
//  UIColor+Demo.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {    
    // black
    static var imBlack: UIColor {
        return UIColor.hex(0x000000)
    }
    static var imLightBlack: UIColor {
        return UIColor.hex(0x545454)
    }
    
    // white
    static var imWhite02: UIColor {
        return UIColor.white.withAlphaComponent(0.2)
    }
    
    static var imWhite08: UIColor {
        return UIColor.white.withAlphaComponent(0.8)
    }
    
    // blue
    static var imBlue: UIColor {
        return UIColor.hex(0x1e8cff)
    }
    static var imDarkBlue: UIColor {
        return UIColor.hex(0x4a90e2)
    }
    
    // grey
    static var imNavigationColor: UIColor {
        return UIColor.hex(0x2b2b2b)
    }
    static var imBackgroundColor: UIColor {
        return UIColor.hex(0xFAFAFC)
    }
    static var imGrey: UIColor {
        return UIColor.hex(0xDEDEDE)
    }
    static var imDarkGrey: UIColor {
        return UIColor.hex(0x555555)
    }
    static var imLightGrey: UIColor {
        return UIColor.hex(0xEBEBF0)
    }
    static var imBorderGrey: UIColor {
        return UIColor.hex(0xc8c8c8)
    }
    static var imShadow: UIColor {
        return UIColor.hex(0x969696)
    }
    static var imUltraGrey: UIColor {        
        return UIColor(red: 208.0/255.0, green: 208.0/255.0, blue: 219.0/255.0, alpha: 1.0)
    }
    
    static var imSectionTitleGrey: UIColor {
        return UIColor(red: 136.0/255.0, green: 136.0/255.0, blue: 152.0/255.0, alpha: 1.0)
    }
    
    static var imSectionGrey: UIColor {
        return UIColor(red: 235.0/255.0, green: 235.0/255.0, blue: 240.0/255.0, alpha: 1.0)
    }
    
    static var imTextLightGray: UIColor {
        return UIColor.hex(0x888898)
    }
    
    // green
    static var imGreen: UIColor {
        return UIColor.hex(0x13ce67)
    }
    
    // red
    static var imRed: UIColor {
        return UIColor.hex(0xFF0066)
    }
}
