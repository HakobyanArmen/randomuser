//
//  UIView+Animation.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation
import UIKit

extension UIView {    
    func fadeIn(_ duration: Double) {
        fadeIn(duration, 0)
    }
    
    func fadeOut(_ duration: Double) {
        fadeOut(duration, 0)
    }
    
    func fadeIn(_ duration: Double, _ delay: Double) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
        }, completion: nil)
    }
    
    func fadeOut(_ duration: Double, _ delay: Double) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.alpha = 0.0
        }, completion: nil)
    }
    
    func pulsate(_ scale: CGFloat = 0.95) {
        UIView.animate(withDuration: 0.3,
                       animations: {
                        self.transform = CGAffineTransform(scaleX: scale, y: scale)
        },
                       completion: { _ in
                        UIView.animate(withDuration: 0.1) {
                            self.transform = CGAffineTransform.identity
                        }
        })
    }
}
