//
//  Driver+Convenience.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

extension Observable where Element == Bool {
    func onlyIfTrue() -> Observable<Void> {
        return self.filter { $0 }.mapVoid()
    }
}

extension SharedSequence where Element == Bool {
    func onlyIfTrue() -> SharedSequence<S, Void> {
        return self.filter { $0 }.mapVoid()
    }
}

extension Observable where Element == Bool {
    func onlyIfFalse() -> Observable<Void> {
        return self.filter { !$0 }.mapVoid()
    }
}

extension SharedSequence where Element == Bool {
    func onlyIfFalse() -> SharedSequence<S, Void> {
        return self.filter { !$0 }.mapVoid()
    }
}

extension SharedSequence {
    func mapVoid() -> SharedSequence<S, Void> {
        return self.map { _ in }
    }
    
    func unwrap<T>() -> SharedSequence<S, T> where E == T? {
        return self.filter { $0 != nil }.map { $0! }
    }
}

