//
//  Observable+Extension.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

extension ObservableType {    
    func catchErrorJustComplete() -> Observable<E> {
        return catchError { _ in
            return Observable.empty()
        }
    }
    
    func asDriverOnErrorJustComplete() -> Driver<E> {
        return asDriver { error in
            return Driver.empty()
        }
    }
    
    func mapVoid() -> Observable<()> {
        return map { _ in return }
    }
}

extension ObservableType {
    func asCompletable() -> Completable {
        return ignoreElements()
    }
}

extension PrimitiveSequence where Trait == SingleTrait {
    func asCompletable() -> Completable {
        return asObservable().asCompletable()
    }
}

extension PrimitiveSequence where Trait == MaybeTrait {
    func asCompletable() -> Completable {
        return asObservable().asCompletable()
    }
}
