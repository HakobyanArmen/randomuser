//
//  Constants.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation

typealias RLocalized = R.string.localizable
typealias RImage = R.image
typealias RNib = R.nib

struct Constant {    
//    static let buttonHeight = 0.0
}
