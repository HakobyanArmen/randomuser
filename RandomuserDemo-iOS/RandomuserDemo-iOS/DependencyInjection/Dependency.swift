//
//  Dependency.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation
import Swinject

final class Dependency {
    
    private var assembler: Assembler!
    
    var resolver: Resolver {
        return assembler.resolver
    }
    
    static var shared: Dependency = {
        return Dependency()
    }()
    
    private init() { }
    
    func initialize() {
        self.assembler = Assembler([
                                    // View Models
                                    GeneralViewModelAssembly(),
                                    // View Controllers
                                    GeneralViewControllersAssembly(),
                                    // Others
                                    NetworkAssembly(),
                                    ServicesAssembly(),
                                    OtherAssembly()
                                    ])
    }
}
