//
//  OtherAssembly.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation
import Swinject

final class OtherAssembly: Assembly {
    
    func assemble(container: Container) {        
        container.register(CoordinatorType.self) { (_) in
            return AppCoordinator()
            }.inObjectScope(.container)
    }
}
