//
//  NetworkAssembly.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation
import Alamofire
import Swinject
import SwinjectAutoregistration

final class NetworkAssembly: Assembly {
    
    func assemble(container: Container) {
        container.register(NetworkProviderConfigurable.self) { r in
            return NetworkProviderConfiguration()
        }
        
        // Singleton
        container.register(NetworkProviderType.self) { (r) in
            let networkProvider = NetworkAPIProvider(config: r.resolve(NetworkProviderConfigurable.self) as! NetworkProviderConfiguration,
                                                     adapter: r.resolve(RequestAdapter.self))
            networkProvider.coordinator = r.resolve(CoordinatorType.self)
            return networkProvider
            }
            .inObjectScope(.container)
    }
}
