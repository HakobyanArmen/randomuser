//
//  GeneralViewModelAssembly.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import UIKit
import Swinject

class GeneralViewModelAssembly: Assembly {
    
    func assemble(container: Container) {
        // Menu
        container.register(MenuViewModelType.self) { resolver in
            let vm = MenuViewModel()
            vm.coordinator = resolver.resolve(CoordinatorType.self)
            return vm
        }
        
        // User Table
        container.register(UserTableViewModelType.self) { resolver in
            let vm = UserTableViewModel(discoveryService:resolver.resolve(DiscoveryServiceType.self)!)
            vm.coordinator = resolver.resolve(CoordinatorType.self)
            return vm
        }
        
        // User Collection
        container.register(UserCollectionViewModelType.self) { resolver in
            let vm = UserCollectionViewModel(discoveryService:resolver.resolve(DiscoveryServiceType.self)!)
            vm.coordinator = resolver.resolve(CoordinatorType.self)
            return vm
        }
        
        // User Deatil
        container.register(DetailViewModelType.self) { resolver in
            let vm = DetailViewModel()
            vm.coordinator = resolver.resolve(CoordinatorType.self)
            return vm
        }
    }
}
