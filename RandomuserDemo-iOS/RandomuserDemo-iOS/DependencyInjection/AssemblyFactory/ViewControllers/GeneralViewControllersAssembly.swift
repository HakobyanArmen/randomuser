//
//  GeneralViewControllersAssembly.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import UIKit
import Swinject

class GeneralViewControllersAssembly: Assembly {
    
    func assemble(container: Container) {
        // MARK: Menu
        container.register(MenuViewController.self) { r in
            return MenuViewController(viewModel: r.resolve(MenuViewModelType.self)!)
        }
        
        // MARK: User Table
        container.register(UserTableViewController.self) { r in
            return UserTableViewController(viewModel: r.resolve(UserTableViewModelType.self)!)
        }
        
        // MARK: User Collection
        container.register(UserCollectionViewController.self) { r in
            return UserCollectionViewController(viewModel: r.resolve(UserCollectionViewModelType.self)!)
        }
        
        // MARK: User Deatil
        container.register(DetailViewController.self) { r in
            return DetailViewController(viewModel: r.resolve(DetailViewModelType.self)!)
        }
    }
}
