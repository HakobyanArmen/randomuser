//
//  ServicesAssembly.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation
import Swinject

final class ServicesAssembly: Assembly {
    
    func assemble(container: Container) {
        // sourcery:inline:Services.AutoInject
        container.register(DiscoveryServiceType.self) { r in
            return DiscoveryService(provider: r.resolve(NetworkProviderType.self)!)
            }.inObjectScope(.container)

    }
}
