//
//  DiscoveryService.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//


import RxSwift
import RxSwiftExt

protocol DiscoveryServiceType: ServiceProtocol {
    func fetchUsers(page: Int64, result: Int64) -> Single<DiscoveryUsers>
}

// sourcery:autoinject
final class DiscoveryService: DiscoveryServiceType {

    private let provider: NetworkProviderType
    
    init(provider: NetworkProviderType) {
        self.provider = provider
    }
    
    var users: BehaviorSubject<[DiscoveryUser]?> = BehaviorSubject(value: nil)
    var suggestionsReadiness: BehaviorSubject<Float> = BehaviorSubject(value: 0.0)
    
    func fetchUsers(page: Int64, result: Int64) -> Single<DiscoveryUsers> {
        return self.provider.createResponseSingle(route: DiscoveryRouter.users(result: result, page: page))
    }
}
