//
//  DiscoveryRouter.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation
import Alamofire

enum DiscoveryRouter: Routable {
    case users(result: Int64, page:Int64)
    
    var path: String? {
        switch self {
        case .users(let result, let page):
            return "?results=\(result)&page=\(page)"
        }
    }

    var method: HTTPMethod {
        switch  self {
        case .users( _, _):
            return .get
        }
    }
}
