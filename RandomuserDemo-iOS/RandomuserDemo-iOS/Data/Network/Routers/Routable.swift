//
//  Routable.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation
import Alamofire

public enum ContentType: String {
    case json = "application/json"
    case formUrlencoded     = "application/x-www-form-urlencoded"
    case multipart = "multipart/form-data"
}

protocol Routable: URLRequestConvertible {
    var isAbsolute: String? { get }
    var baseURLString: String { get }
    var method: HTTPMethod { get }
    var pathPrefix: String { get }
    var apiVersion: APIVersion { get }
    var path: String? { get }
    var responseRootKey: String? { get }
    var encoder: ParameterEncoding { get }
    var params: Parameters? { get }
    var headers: [String: String]? { get }
    var contentType: ContentType { get }
    var file: Data? { get }
    var requiresAuthentication: Bool { get }
}

extension Routable {
    var isAbsolute: String? {
        return nil
    }
    
    var baseURLString: String {
        return AppEnvironment.current.baseURL
    }
    
    var apiVersion: APIVersion {
        return .v1
    }
    
    var pathPrefix: String {
        return ""
    }
    
    var path: String? {
        return nil
    }
    
    var encoder: ParameterEncoding {
        if method == .get {
            return URLEncoding.default
        } else {
            // #dev This will make login request work. A.H.
            if contentType == .formUrlencoded {
                return URLEncoding.default
            }

            return JSONEncoding.default
        }
    }
    
    var responseRootKey: String? {
        return nil
    }
    
    var params: Parameters? {
        return nil
    }
    
    var headers: [String: String]? {
        return nil
    }
    
    var requiresAuthentication: Bool {
        return true
    }
    
    var contentType: ContentType {
        return .json
    }
    
    var file: Data? {
        return nil
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try self.baseURLString.asURL()
        var urlString = url.absoluteString
        
        if let absoluteURl = self.isAbsolute {
            urlString = absoluteURl
        }
        else {            
            if !pathPrefix.isEmpty { urlString += "/\(pathPrefix)" }
            if let path = self.path {
                urlString += "/\(path)"
            }
            
            guard let urlstr = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
                throw ErrorV1.error(withReason: .invalidURL(url: urlString))
            }
            urlString = urlstr
        }
        guard let validURL = URL(string: urlString) else {
            throw ErrorV1.error(withReason: .invalidURL(url: urlString))
        }
        
        var urlRequest = URLRequest(url: validURL)
        urlRequest.httpMethod = self.method.rawValue
        urlRequest = try self.encoder.encode(urlRequest, with: self.params)
        
        var headers = defaultHTTPHeaders()
        if let customHeaders = self.headers {
            headers.merge(customHeaders, uniquingKeysWith: { $1 })
        }
        urlRequest.allHTTPHeaderFields = headers
        
        return urlRequest
    }
    
    private func defaultHTTPHeaders() -> [String: String] {
        var headers = [String: String]()
        let environment = AppEnvironment.current!

        // #dev configure Default headers. A.H.
        headers["X-API-VERSION"] = self.apiVersion.rawValue
        headers["X-DEVICE-TYPE"] = "i"
        headers["X-CLIENT-VERSION"] = environment.appVersion
        headers["X-DEVICE-LANGUAGE"] = environment.systemLanguage
        headers["X-DEVICE-ID"] = environment.deviceNotificationToken
        headers["Content-Type"] = self.contentType.rawValue
        return headers
    }
}

enum APIVersion: String {
    case none = ""
    case v1 = "0.1.0"
}

