//
//  RxAlamofireResponse.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

extension DataRequest {
    func responseSingle<T: DecodableModel>(queue: DispatchQueue? = nil, keyPath: String? = nil) -> Single<T>  {
        return Single<T>.create { (single) -> Disposable in
            let request = self.responseDecodableObject(queue: queue, keyPath: keyPath, decoder: jsonDecoder) { (response: DataResponse<T>) in
                switch response.result {
                case let .failure(err):
                    single(.error(err))
                case let .success(val):
                    single(.success(val))
                }
            }
            request.resume()
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func responseCollectionSingle<T: DecodableModel>(queue: DispatchQueue? = nil, keyPath: String? = nil) -> Single<[T]>  {
        return Single<[T]>.create { (single) -> Disposable in
            let request = self.responseDecodableSequence(queue: queue, keyPath: keyPath, decoder: jsonDecoder) { (response: DataResponse<[T]>) in
                switch response.result {
                case let .failure(err):
                    single(.error(err))
                case let .success(val):
                    single(.success(val))
                }
            }
            request.resume()
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func responseCompletable(queue: DispatchQueue? = nil) -> Completable {
        return Completable.create(subscribe: { (completable) -> Disposable in            
            let request = self.response(completionHandler: { (dataResponse) in
                Log.network("[REQUEST] \(dataResponse.request?.url?.absoluteString ?? "")")
                
                if let err = dataResponse.error {
                    Log.network("[ERROR] \(String(describing: dataResponse.error))")
                    completable(.error(err))
                } else if (dataResponse.response?.statusCode ?? 0) > 299 {
                    let apierrorResult: Result<APIError> = DataRequest.decodeToObject(decoder: jsonDecoder, response: dataResponse.response, data:dataResponse.data)
                    
                    switch apierrorResult {
                    case let .success(error):
                        Log.network("[ERROR] \(String(describing: error))")
                        completable(.error(error))
                        
                    case let .failure(error):
                        Log.network("[ERROR] \(String(describing: error))")
                        completable(.error(error))
                    }
                } else {
                    if let bytes = dataResponse.data,
                        let responseBody = String(data: bytes, encoding: .utf8) {
                        Log.network("[RESPONSE] \(responseBody)")
                        completable(.completed)
                    } else {
                        completable(.error(ErrorV1.error(withReason: .general)))
                        Log.network("[RESPONSE] nil body")
                    }
                }
            })
            request.resume()
            return Disposables.create {
                request.cancel()
            }
        })
    }
}
