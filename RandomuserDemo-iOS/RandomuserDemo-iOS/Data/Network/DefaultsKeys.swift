//
//  DefaultsKeys.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation

extension DefaultsKeys {
    static let apiRefreshToken = DefaultsKey<String?>("refresh_token")
    static let apiAuthToken = DefaultsKey<String?>("auth_token")    
    static let baseURL = DefaultsKey<String>("baseURL")
}
