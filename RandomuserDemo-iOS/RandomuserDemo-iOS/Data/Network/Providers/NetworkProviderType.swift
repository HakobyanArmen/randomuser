//
//  DataProviderType.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

/// Main protocol to govern data communication. Could be coming from API, Database, Different API, Streaming server, etc...
protocol DataProviderType {}

protocol NetworkProviderType: DataProviderType {
    func createResponseSingle<T: DecodableModel, R: Routable>(route: R) -> Single<T>
    func createResponseSingle<T: DecodableModel, R: Routable>(route: R) -> Single<[T]>
    func createResponseCompletable<R: Routable>(route: R) -> Completable
}
