//
//  NetworkAPIProvider.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift
import RxAlamofire
import RxCocoa
import Alamofire_Synchronous
import SwiftyJSON

final class NetworkAPIProvider: NetworkProviderType {
    var sessionManager: SessionManager? = nil
    var authenticationAdapter: RequestAdapter?
    
    var coordinator: CoordinatorType?

    init(config: NetworkProviderConfiguration) {
        sessionManager = SessionManager(configuration: config.sessionConfig)
        sessionManager?.startRequestsImmediately = false
    }
    
    convenience init(config: NetworkProviderConfiguration, adapter: RequestAdapter?) {
        self.init(config: config)
        authenticationAdapter = adapter
    }
    
    // RxObservables
    func createResponseSingle<T: DecodableModel, R: Routable>(route: R) -> Single<T> {
        guard let sessionManager = self.sessionManager else {
            return Single.error(ErrorV1.error(withReason: .sessionUnavailable))
        }
        
        return sessionManager
            .request(route)
            .responseSingle(keyPath: route.responseRootKey)
            .do(onError: errorSideEffect(_:))
    }
    
    func createResponseSingle<T: DecodableModel, R: Routable>(route: R) -> Single<[T]> {
        guard let sessionManager = self.sessionManager else {
            return Single.error(ErrorV1.error(withReason: .sessionUnavailable))
        }

        return sessionManager
            .request(route)
            .responseCollectionSingle(keyPath: route.responseRootKey)
            .do(onError: errorSideEffect(_:))
    }
    
    func createResponseCompletable<R: Routable>(route: R) -> Completable {
        guard let sessionManager = self.sessionManager else {
            return Completable.error(ErrorV1.error(withReason: .sessionUnavailable))
        }

        return sessionManager
            .request(route)
            .responseCompletable()
            .do(onError: errorSideEffect(_:))
    }
    
    func errorSideEffect(_ err: Error) {
        // #dev we need to logout no unauthenticated users here. A.H.
        if let apiErr = err as? ErrorV1 {
            if case .unauthenticatedRequest = apiErr.reason {
                if Defaults[.apiAuthToken] != nil {
//                    self.coordinator?.execute(step: AppStep.logout)
//                    self.coordinator?.execute(step: AppStep.authentication)
                }
            }
        }
    }
}

