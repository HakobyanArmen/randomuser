//
//  NetworkProviderConfiguration.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation
import Alamofire

protocol NetworkProviderConfigurable: ProviderConfigurable {
    var sessionConfig: URLSessionConfiguration { get set }
    
    init(sessionConfiguration: URLSessionConfiguration)
}

struct NetworkProviderConfiguration: NetworkProviderConfigurable {
    var sessionConfig: URLSessionConfiguration
    
    /// Provides an initializer and default values for the required properties.
    init(sessionConfiguration: URLSessionConfiguration = URLSessionConfiguration.default) {        
        self.sessionConfig = sessionConfiguration
    }
}
