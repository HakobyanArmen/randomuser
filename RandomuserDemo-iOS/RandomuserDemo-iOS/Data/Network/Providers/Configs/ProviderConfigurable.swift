//
//  ProviderConfigurable.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation
import Alamofire

protocol ProviderConfigurable {}

struct EmptyProviderConfiguration: ProviderConfigurable {}
