//
//  ErrorViewController.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import UIKit

typealias ErrorActionClosure = (_ controller: UIViewController) -> Void

class ErrorViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    
    // MARK: - Variables
    fileprivate let titleText: String
    fileprivate let action: ErrorActionClosure?
    
    // MARK: - Initializers
    init(title: String, action: ErrorActionClosure? = nil) {
        self.titleText = title
        self.action = action
        
        super.init(nibName: "ErrorViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError(.wrongInit)
    }
    
    // MARK: - Private Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        BaseStyles.background.apply(to: view)
        BaseStyles.rounded().styling(actionButton)
        
        titleLabel.text = titleText
        actionButton.setTitle(RLocalized.button_try_again(), for: .normal)
    }
    
    // MARK: - Actions
    @IBAction func tapOnAction(_ sender: Any) {
        action?(self)
    }
}
