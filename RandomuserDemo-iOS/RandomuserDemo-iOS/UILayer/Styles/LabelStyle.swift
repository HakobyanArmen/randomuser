//
//  LabelStyle.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation
import UIKit

struct LabelStyle {    
    static func base(with size: CGFloat) -> UIViewStyle<UILabel> {
        return UIViewStyle { label in
            label.font = Font.regular(size)
        }
    }
    
    static func baseBold(with size: CGFloat) -> UIViewStyle<UILabel> {
        return UIViewStyle { label in
            label.font = Font.bold(size)
        }
    }
    
    static let big: UIViewStyle<UILabel> = base(with: 30)
    static let bigBold: UIViewStyle<UILabel> = baseBold(with: 30)
    static let base: UIViewStyle<UILabel> = LabelStyle.base(with: 18)
    static let baseBold: UIViewStyle<UILabel> = LabelStyle.baseBold(with: 18)
    static let small: UIViewStyle<UILabel> = base(with: 14)
    static let smallBold: UIViewStyle<UILabel> = baseBold(with: 14)
    
    static let blue: UIViewStyle<UILabel> = UIViewStyle { label in
        label.textColor = UIColor.imBlue
    }
    
    static let darkGrey: UIViewStyle<UILabel> = UIViewStyle { label in
        label.textColor = UIColor.imDarkGrey
    }
    
    static let grey: UIViewStyle<UILabel> = UIViewStyle { label in
        label.textColor = UIColor.imGrey
    }
    
    static let black: UIViewStyle<UILabel> = UIViewStyle { label in
        label.textColor = UIColor.black
    }
    
    static let white: UIViewStyle<UILabel> = UIViewStyle { label in
        label.textColor = UIColor.white
    }
    
    static let dialogTitle: UIViewStyle<UILabel> =  UIViewStyle { label in
        LabelStyle.baseBold.styling(label)
        LabelStyle.blue.styling(label)
    }
    
    static let dialogText: UIViewStyle<UILabel> =  UIViewStyle { label in
        LabelStyle.small.styling(label)
        LabelStyle.black.styling(label)
    }
}

