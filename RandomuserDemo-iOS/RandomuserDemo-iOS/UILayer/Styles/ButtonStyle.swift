//
//  ButtonStyle.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//


import Foundation
import UIKit

struct ButtonStyle {
    static func base<V: UIButton>(cornerRadius: CGFloat = 0) -> UIViewStyle<V> {
        return BaseStyles.rounded(cornerRadius: cornerRadius).composing { (button: UIButton) in
            button.setTitleColor(UIColor.white, for: .normal)
            button.backgroundColor = UIColor.imBlue
        }
    }
    
    static let rounded: UIViewStyle<UIButton> = base(cornerRadius: 4)
    
    static func bold<V: UIButton>(ofSize: CGFloat = 22) -> UIViewStyle<V> {
        return UIViewStyle { button in
            guard let label = button.titleLabel else {
                return
            }
            LabelStyle.baseBold(with: ofSize).apply(to: label)
        }
    }
    
    static func regular<V: UIButton>(ofSize size: CGFloat = 18) -> UIViewStyle<V> {
        return UIViewStyle { button in
            guard let label = button.titleLabel else { return }            
            LabelStyle.base(with: size).apply(to: label)
        }
    }
    
    static let clear: UIViewStyle<UIButton> = rounded.composing { (button: UIButton) in
        button.backgroundColor = UIColor.clear
        button.setTitleColor(UIColor.white, for: .normal)
    }
}
