//
//  RootCoordinator.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation
import UIKit
import MessageUI

final class RootCoordinator: CoordinatorType, CoordinatorDelegate {
    
    var childCoordinators: [CoordinatorType] = []
    
    weak var delegate: CoordinatorDelegate?
    let navigationController: UINavigationController
    
    init(navigationVC: UINavigationController, delegate: CoordinatorDelegate? = nil) {
        self.navigationController = navigationVC
        self.delegate = delegate
    }
    
    // NOTE: Populate if you need a contact between view models or view controllers Use this
    func execute(step: CoordinatorStepType, notifier: NotifierType) -> Bool { return true }
    
    func execute(step: CoordinatorStepType) -> Bool {
        guard let rootStep = step as? RootStep else {
            return self.cascade(step: step)
        }
        switch rootStep {
        case .table                             :showTable()
        case .collection                        :showCollection()
        case .detail(let user)                  :showDetail(user: user)
        case .email(let email)                  :showEmail(email: email)
        case .phone(let number)                 :showCall(number: number)
        case .cell(let number)                  :showCall(number: number)
        case .otherDetails(let user)            :showOtherDetail(user: user)
        }
        return true
    }
    
    private func showTable() {
        let vc = Dependency.shared.resolver.resolve(UserTableViewController.self)!
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    private func showCollection() {
        let vc = Dependency.shared.resolver.resolve(UserCollectionViewController.self)!
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    private func showDetail(user: DiscoveryUser) {
        let vc = Dependency.shared.resolver.resolve(DetailViewController.self)!
        vc.configure(with: user)
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    private func showEmail(email: String) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self.navigationController
            mail.setToRecipients([email])
            self.navigationController.present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    private func showCall(number: String) {
        if let url = URL(string: "tel://\(number)") {
            UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        }
    }
    
    private func showOtherDetail(user: DiscoveryUser) {
        let vc = Dependency.shared.resolver.resolve(DetailViewController.self)!
        vc.configure(with: user)
        self.navigationController.pushViewController(vc, animated: true)
    }
}

extension UINavigationController: MFMailComposeViewControllerDelegate {
    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
