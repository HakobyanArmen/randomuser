//
//  AppCoordinator.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import NVActivityIndicatorView

final class AppCoordinator: CoordinatorType, CoordinatorDelegate {
    
    var delegate: CoordinatorDelegate?
    var childCoordinators: [CoordinatorType] = []
    var activityIndicatorView: NVActivityIndicatorView? = nil
    
    var window: UIWindow? {
        return UIApplication.shared.keyWindow
    }
    
    var root: UIViewController? {
        get {
            return self.window?.rootViewController
        }
        set {
            self.window?.rootViewController = newValue
        }
    }
    
    // NOTE: Populate if you need a contact between view models or view controllers Use this
    func execute(step: CoordinatorStepType, notifier: NotifierType) -> Bool {
        guard let _ = step as? AppStep else {
            return self.cascade(step: step, notifier: notifier)
        }
        
        return true
    }

    @discardableResult
    func execute(step: CoordinatorStepType) -> Bool {
        guard let appStep = step as? AppStep else {
            return self.cascade(step: step)
        }
        
        // #dev In this switch check, we can group the steps that requires `present` and those that require `push`.
        // And handle those that needs extra params to be passed. A.P        
        switch appStep {
        case .toggleLoading(let isLoading)      :toggleLoading(isLoading: isLoading)
        case .presentError(let error)           :presentError(error: error)
        case .presentMessage(let message)       :presentMessage(message: message)
        case .presentAlert(let message)         :presentAlert(message: message)

        case .load                              :loadMenu()
        }
        return true
    }
    
    
    private func toggleLoading(isLoading: Bool) {
        isLoading ? showLoading() : hideLoading()
    }
    
    fileprivate func showLoading() {
        guard let vc = UIApplication.shared.topMostViewController() else {
            return
        }
        
        // #dev We use a 0.5 second delay to not show an activity indicator
        // in case our data loads very quickly. A.H.
        // Ensure the UI is updated from the main thread in case this method is called from a closure
        DispatchQueue.main.async { // , type: type,
            let indicator = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50),
                                                    type: .circleStrokeSpin,
                                                    color: UIColor.imRed)
            indicator.center = CGPoint(x: vc.view.bounds.size.width/2, y: vc.view.bounds.size.height/2)
            indicator.color = UIColor.imRed
            indicator.startAnimating()
            vc.view.addSubview(indicator)
            
            self.activityIndicatorView = indicator
        }
    }
    
    fileprivate func hideLoading() {
        guard let indicator = self.activityIndicatorView else {
            return
        }
        
        indicator.stopAnimating()
        indicator.removeFromSuperview()
    }
    
    // #dev Handle localization with R.swift. For ex. RLocalized.dialog_title_attention(). A.H.
    private func presentError(title: String = "", error: Error) {
    }

    private func presentMessage(title: String = "", message: String) {
    }

    private func presentAlert(title: String = "", message: String) {

    }
    
    private func loadMenu() {
        let vc = Dependency.shared.resolver.resolve(MenuViewController.self)!
        let navigationVC = BaseNavigationController(rootViewController: vc, isNavigationBarHidden: false)
        let rootCoordinator = RootCoordinator(navigationVC: navigationVC, delegate: self)
        self.add(childCoordinator: rootCoordinator)
        
        self.root = navigationVC
    }

}
