//
//  RootStep.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation

enum RootStep: CoordinatorStepType, Equatable {
    static func == (lhs: RootStep, rhs: RootStep) -> Bool {
        return true
    }
    
    case table
    case collection
    case detail(user: DiscoveryUser)
    case email(email: String)
    case phone(number: String)
    case cell(number: String)
    case otherDetails(user: DiscoveryUser)
}
