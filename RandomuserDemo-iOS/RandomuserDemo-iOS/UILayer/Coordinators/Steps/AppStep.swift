//
//  AppStep.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation
import RxSwift

enum AppStep: CoordinatorStepType {
    case toggleLoading(Bool)
    case presentError(Error)
    case presentMessage(String)
    case presentAlert(String)
    case load
}

