//
//  BaseNavigationController.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation
import UIKit

class BaseNavigationController: UINavigationController {
    
    init(rootViewController: UIViewController, isNavigationBarHidden: Bool = false) {
        super.init(rootViewController: rootViewController)
        
        if isNavigationBarHidden {
            setClearNavigationBar()
        } else {
            setIMMenuNavigationBar()
        }
        
        self.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        // #dev Replace with fatalError in Logger.swift after implementing InnerError. A.H.
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setIMMenuNavigationBar() {
        self.view.backgroundColor = .clear
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.isTranslucent = true
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.tintColor = .imBlack
        self.navigationBar.titleTextAttributes = [.foregroundColor : UIColor.imBlack, .font : Font.semibold(18)]
        
        if #available(iOS 11.0, *) {
            self.navigationBar.largeTitleTextAttributes = [.foregroundColor : UIColor.imBlack, .font : Font.semibold(34)]
            self.navigationBar.prefersLargeTitles = true
        }
    }
    
    private func setClearNavigationBar() {
        self.navigationBar.tintColor = UIColor.clear
        self.navigationBar.isTranslucent = true
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.backgroundColor = UIColor.clear
    }
}

extension BaseNavigationController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        
    }
}

