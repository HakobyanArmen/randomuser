//
//  BaseViewModel.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class BaseViewModel: ViewModelType, ReactiveCompatible {
    
    init() {
        Log.initOf(self)
    }
    
    deinit {
        Log.deinitOf(self)
    }
}
