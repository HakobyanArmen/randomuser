//
//  BaseViewController.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import NVActivityIndicatorView

class BaseViewController: UIViewController, HasDisposeBag, NVActivityIndicatorViewable {
    
    var isRootViewController: Bool {
        return false
    }
    
    var isPrefersLargeTitles: Bool {
        return true
    }
    
    init() {
        super.init(nibName: type(of: self).defaultNib, bundle: nil)
        Log.initOf(self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        // #dev Replace with fatalError in Logger.swift after implementing InnerError. A.H.
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        BaseStyles.background.apply(to: view)
        
        // hide the back button text
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        bindUI()
        bindStyles()
        bindTexts()
        
        
        if let navigation = self.navigationController {
                navigation.navigationBar.prefersLargeTitles = isPrefersLargeTitles
        }
    }
    
    public func bindStyles() {
        
    }
    
    public func bindTexts() {
        
    }
    
    public func bindUI() {
        
    }
    
    deinit {
        Log.deinitOf(self)
    }
}

