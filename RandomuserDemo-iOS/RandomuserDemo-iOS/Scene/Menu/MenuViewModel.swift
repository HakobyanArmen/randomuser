//
//  MenuViewModel.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol MenuViewModelInputs {
    func tableButtonTapped()
    func collectionButtonTapped()
}

protocol MenuViewModelType: ViewModelType {
    var inputs: MenuViewModelInputs { get }
}

class MenuViewModel: BaseViewModel, MenuViewModelInputs, MenuViewModelType {
    
    // MARK: - Public Params
    var coordinator: CoordinatorType?
    
    // MARK: - Init
    override init() {
        super.init()
    }
    
    // MARK: - Inputs
    func tableButtonTapped() {
        self.coordinator?.execute(step: RootStep.table)
    }
    
    func collectionButtonTapped() {
        self.coordinator?.execute(step: RootStep.collection)
    }
    
    // MARK: - MenuViewModelType
    var inputs: MenuViewModelInputs { return self }
}
