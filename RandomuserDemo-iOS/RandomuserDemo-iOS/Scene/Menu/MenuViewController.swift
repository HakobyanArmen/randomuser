//
//  MenuViewController.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import UIKit
import RxSwift

class MenuViewController: BaseViewController {
        
    @IBOutlet weak var tableButton: UIButton!
    @IBOutlet weak var collectionButton: UIButton!

    // MARK: View Controller Life Cycle Methods
    private let viewModel: MenuViewModelType
    init(viewModel: MenuViewModelType) {
        self.viewModel = viewModel
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError(.wrongInit)
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel.bindActivityIndicator()
        self.viewModel.bindErrorTracker()
        
        defer {
            bindViewModel()
        }
    }
    
    override func bindTexts() {
        self.tableButton.setTitle(RLocalized.button_table(), for: .normal)
        self.collectionButton.setTitle(RLocalized.button_collection(), for: .normal)
    }
    
    override func bindStyles() {
        BaseStyles.rounded()
            .composing(with: BaseStyles.border())
            .apply(to: self.tableButton)
        
        BaseStyles.rounded()
            .composing(with: BaseStyles.border())
            .apply(to: self.collectionButton)
    }
    
    // MARK: - Binding
    private func bindViewModel() {
        // Inputs
        self.tableButton.rx.tap.asDriver()
            .drive(onNext: self.viewModel.inputs.tableButtonTapped)
            .disposed(by: self.disposeBag)
        
        self.collectionButton.rx.tap.asDriver()
            .drive(onNext: self.viewModel.inputs.collectionButtonTapped)
            .disposed(by: self.disposeBag)
    }
}
