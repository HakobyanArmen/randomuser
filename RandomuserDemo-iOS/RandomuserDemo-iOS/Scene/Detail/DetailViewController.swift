//
//  DetailViewController.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import UIKit
import RxSwift

class DetailViewController: BaseViewController {
    
    override var isPrefersLargeTitles: Bool { return false }

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var stateLbl: UILabel!
    @IBOutlet weak var emailBtn: UIButton!
    @IBOutlet weak var phoneBtn: UIButton!
    @IBOutlet weak var cellBtn: UIButton!
    @IBOutlet weak var otherDeatilsBtn: UIButton!

    // MARK: View Controller Life Cycle Methods
    private let viewModel: DetailViewModelType
    init(viewModel: DetailViewModelType) {
        self.viewModel = viewModel
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError(.wrongInit)
    }
    
    func configure(with user: DiscoveryUser) {
        self.viewModel.inputs.configure(with: user)
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel.bindActivityIndicator()
        self.viewModel.bindErrorTracker()
        
        defer {
            bindViewModel()
        }
    }
    
    override func bindTexts() {
        self.title  = RLocalized.title_detail()
        self.otherDeatilsBtn.setTitle(RLocalized.button_other_details(), for: .normal)
    }
    
    override func bindStyles() {
        BaseStyles.rounded()
            .composing(with: BaseStyles.border())
            .apply(to: self.emailBtn)

        BaseStyles.rounded()
            .composing(with: BaseStyles.border())
            .apply(to: self.phoneBtn)
        
        BaseStyles.rounded()
            .composing(with: BaseStyles.border())
            .apply(to: self.cellBtn)
    }
    
    // MARK: - Binding
    private func bindViewModel() {
        // Inputs
        self.emailBtn.rx.tap.asDriver()
            .drive(onNext: self.viewModel.inputs.emailButtonTapped)
            .disposed(by: self.disposeBag)
        
        self.phoneBtn.rx.tap.asDriver()
            .drive(onNext: self.viewModel.inputs.phoneButtonTapped)
            .disposed(by: self.disposeBag)
        
        self.cellBtn.rx.tap.asDriver()
            .drive(onNext: self.viewModel.inputs.cellButtonTapped)
            .disposed(by: self.disposeBag)
        
        self.otherDeatilsBtn.rx.tap.asDriver()
            .drive(onNext: self.viewModel.inputs.otherDetailButtonTapped)
            .disposed(by: self.disposeBag)
        
        // Outputs
        self.viewModel.outputs.userPreview.asObservable()
            .bind { user in
                if let imageUrl = user?.picture?.large {
                    self.imageView.sd_setImage(with: URL(string: imageUrl)!)
                }
                
                var name: String = ""
                if let f = user?.name?.first {
                    name = f
                }
                if let l = user?.name?.last {
                    name = name + " " + l
                }

                self.nameLbl.text = name
                self.addressLbl.text = user?.location?.street
                self.cityLbl.text = user?.location?.city
                self.stateLbl.text = user?.location?.state
                
                if let email = user?.email {
                    self.emailBtn.setTitle("Send email to " + email, for: .normal)
                } else {
                    self.emailBtn.isEnabled = false
                }
                
                if let phone = user?.phone {
                    self.phoneBtn.setTitle("Call " + phone, for: .normal)
                } else {
                    self.phoneBtn.isEnabled = false

                }
                
                if let cell = user?.cell  {
                    self.cellBtn.setTitle("Call " + cell, for: .normal)
                } else {
                    self.cellBtn.isEnabled = false
                }
                
            }
            .disposed(by: self.disposeBag)
    }
}

