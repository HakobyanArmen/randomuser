//
//  DetailViewModel.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol DetailViewModelInputs {
    func configure(with user: DiscoveryUser)

    func emailButtonTapped()
    func phoneButtonTapped()
    func cellButtonTapped()
    func otherDetailButtonTapped()
}

protocol DetailViewModelOutputs {
    var userPreview: Driver<DiscoveryUser?> { get }
}

protocol DetailViewModelType: ViewModelType {
    var inputs: DetailViewModelInputs { get }
    var outputs: DetailViewModelOutputs { get }
}

class DetailViewModel: BaseViewModel, DetailViewModelInputs, DetailViewModelType, DetailViewModelOutputs {
    
    // MARK: - Public Params
    var coordinator: CoordinatorType?
    
    // MARK: - Init
    override init() {
        super.init()
    }
    
    // MARK: - Inputs
    func configure(with user: DiscoveryUser) {
        self.userPreviewRelay.accept(user)
    }
    
    func otherDetailButtonTapped() {
        self.coordinator?.execute(step: RootStep.otherDetails(user: self.userPreviewRelay.value!))
    }
    
    func emailButtonTapped() {
        let user = self.userPreviewRelay.value!
        if user.email != nil {
            self.coordinator?.execute(step: RootStep.email(email: user.email!))
        }
    }
    
    func phoneButtonTapped() {
        let user = self.userPreviewRelay.value!
        if user.phone != nil {
            self.coordinator?.execute(step: RootStep.phone(number: user.phone!))
        }
    }
    
    func cellButtonTapped() {
        let user = self.userPreviewRelay.value!
        if user.cell != nil {
            self.coordinator?.execute(step: RootStep.cell(number: user.cell!))
        }
    }

    
    // MARK: - Outputs
    private let userPreviewRelay = BehaviorRelay<DiscoveryUser?>(value: nil)
    private(set) lazy var userPreview: Driver<DiscoveryUser?> = {
        return userPreviewRelay.asDriver()
    }()
    
    // MARK: - DetailViewModelType
    var inputs: DetailViewModelInputs { return self }
    var outputs: DetailViewModelOutputs { return self }
}
