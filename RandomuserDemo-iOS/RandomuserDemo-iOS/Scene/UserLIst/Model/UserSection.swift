//
//  UserSection.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation
import RxDataSources

struct UserSection {
    var items: [Item]
}

extension UserSection: SectionModelType {
    typealias Item = DiscoveryUser
    
    init(original: UserSection, items: [Item]) {
        self = original
        self.items = items
    }
}
