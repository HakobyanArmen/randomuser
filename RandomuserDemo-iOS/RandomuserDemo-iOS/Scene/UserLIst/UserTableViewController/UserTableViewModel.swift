//
//  UserTableViewModel.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol UserTableViewModelInputs {
    func viewDidLoad()
    func userSelected(index: Int)
}

protocol UserTableViewModelOutputs {
    var usersDriver: Driver<[UserSection]> { get }
    var isEmpty: Observable<Void> { get }
    var isError: Observable<Void> { get }
}

protocol UserTableViewModelType: ViewModelType {
    var inputs: UserTableViewModelInputs { get }
    var outputs: UserTableViewModelOutputs { get }
}

final class UserTableViewModel: BaseViewModel, UserTableViewModelType, UserTableViewModelOutputs, UserTableViewModelInputs {

    // MARK: - Public Params
    var coordinator: CoordinatorType?
    
    // MARK: - Private Params
    private var discoveryService: DiscoveryServiceType

    private var discoveryUsersRelay = BehaviorRelay<[DiscoveryUser]>(value: [])

    // MARK: - Init
    init(discoveryService: DiscoveryServiceType) {
        self.discoveryService = discoveryService
        super.init()
        
        bind()
    }
    
    // MARK: - Bind
    private func bind() {
        
    }
    
    // MARK: - Inputs
    func viewDidLoad() {
        self.discoveryService
            .fetchUsers(page: 0, result: 100)
            .trackActivity(self.activityIndicator)
            .trackError(self.errorTracker)
            .flatMap({ data -> Observable<[DiscoveryUser]> in
                guard let users = data.users, users.count > 0 else {
                    self.isEmptyRelay.accept(true)
                    return Observable.just([])
                }
                let sortedUsers = users.sorted(by: { $0.name!.first! < $1.name!.first! })
                self.discoveryUsersRelay.accept(sortedUsers)
                return Observable.just(sortedUsers)
            })
            .map { [unowned self] users in
                self.convertUsers(users)
            }
            .subscribe(onNext: { users in
                self.usersRelay.accept(users)
            }, onError: { error in
                self.isErrorRelay.accept(true)
            })
            .disposed(by: self.disposeBag)
    }
    
    func userSelected(index: Int) {
        self.coordinator?.execute(step: RootStep.detail(user: self.discoveryUsersRelay.value[index]))
    }
    
    // MARK: - Outputs
    private var usersRelay = BehaviorRelay<[UserSection]>(value: [])
    private(set) lazy var usersDriver: Driver<[UserSection]> = {
        return self.usersRelay.asDriver(onErrorJustReturn: [])
    }()
    
    private let isEmptyRelay = BehaviorRelay<Bool>(value: false)
    var isEmpty: Observable<Void> {
        return self.isEmptyRelay.asObservable().onlyIfTrue()
    }
    
    private let isErrorRelay = BehaviorRelay<Bool>(value: false)
    var isError: Observable<Void> {
        return self.isErrorRelay.asObservable().onlyIfTrue()
    }
    
    // MARK: Helpers
    private func convertUsers(_ users: [DiscoveryUser]) -> [UserSection] {
        return [UserSection(items: users)]
    }
    // MARK: - UserTableViewModelType
    var inputs: UserTableViewModelInputs { return self }
    var outputs: UserTableViewModelOutputs { return self }
}

