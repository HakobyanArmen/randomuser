//
//  UserTableViewController.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class UserTableViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    override var isPrefersLargeTitles: Bool { return true }

    fileprivate var isEmpty: Bool = false
    fileprivate var isError: Bool = false
    
    fileprivate let viewModel: UserTableViewModelType
    init(viewModel: UserTableViewModelType) {
        self.viewModel = viewModel
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError(.wrongInit)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel.bindActivityIndicator()
        self.viewModel.bindErrorTracker()
        self.viewModel.inputs.viewDidLoad()
        
        setupCollectionView()
        
        defer {
            bindViewModel()
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func bindTexts() {
        self.title = "COLLECTION" //RLocalized.title_likes()
    }
    
    override func bindStyles() {
    }
    
    // MARK: - Binding
    private func bindViewModel() {
        // Outputs
        self.viewModel.outputs.usersDriver
            .drive(self.tableView.rx.items(dataSource: dataSource()))
            .disposed(by: disposeBag)
        
        self.viewModel.outputs.isEmpty
            .bind(to: self.rx.isEmpty)
            .disposed(by: self.disposeBag)
        
        self.viewModel.outputs.isError
            .bind(to: self.rx.isError)
            .disposed(by: self.disposeBag)
        
        // Inputs
        tableView.rx.itemSelected
            .asDriver()
            .drive(onNext: { [weak self] (indexPath) in
                self?.viewModel.inputs.userSelected(index: indexPath.row)
            })
            .disposed(by: self.disposeBag)
    }
    
    fileprivate func setupCollectionView() {
        tableView.register(RNib.userTableViewCell(),
                           forCellReuseIdentifier: R.reuseIdentifier.userTableViewCell.identifier)
        tableView.rx.setDelegate(self)
            .disposed(by: disposeBag)
    }
}

extension Reactive where Base: UserTableViewController {
    var isEmpty: AnyObserver<Void> {
        return Binder<Void>(self.base) { controller, isEmpty in
            let emptyVC = EmptyViewController(title: RLocalized.text_list_empty(), action: { vc in
                vc.remove()
                controller.viewModel.inputs.viewDidLoad()
            })
            
            controller.add(emptyVC)
            
            }.asObserver()
    }
}

extension Reactive where Base: UserTableViewController {
    var isError: AnyObserver<Void> {
        return Binder<Void>(self.base) { controller, isError in
            let errorVC = ErrorViewController(title: RLocalized.text_list_error(), action: { vc in
                vc.remove()
                controller.viewModel.inputs.viewDidLoad()
            })
            
            controller.add(errorVC)
            
            }.asObserver()
    }
}

// MARK: - RxDataSource
extension UserTableViewController {
    fileprivate func dataSource() -> RxTableViewSectionedReloadDataSource<UserSection> {
        return RxTableViewSectionedReloadDataSource<UserSection>(configureCell: { dataSource, tableView, indexPath, item in
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.userTableViewCell, for: indexPath)!
            cell.configure(with: item)
            return cell
        }
        )
    }
}

// MARK: - UITableViewDelegate
extension UserTableViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 84
    }
}
