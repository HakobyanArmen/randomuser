//
//  UserTableViewCell.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        BaseStyles.rounded(cornerRadius: 12).apply(to: self)
    }
    
    func configure(with user: DiscoveryUser) {
        if let imageUrl = user.picture?.medium {
            self.imgView.sd_setImage(with: URL(string: imageUrl)!)
        }
        
        var name: String = ""
        if let f = user.name?.first {
            name = f
        }
        if let l = user.name?.last {
            name = name + " " + l
        }
        
        self.nameLbl.text = name
        
        var address: String = ""
        if let c = user.location?.city {
            address = c
        }
        if let s = user.location?.street {
            address = address + "," + s
        }
        
        self.addressLbl.text = address
        self.phoneLbl.text = user.phone
    }
}
