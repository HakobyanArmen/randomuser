//
//  UserCollectionViewController.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class UserCollectionViewController: BaseViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    override var isPrefersLargeTitles: Bool { return true }
    
    fileprivate var isEmpty: Bool = false
    fileprivate var isError: Bool = false

    fileprivate let viewModel: UserCollectionViewModelType
    init(viewModel: UserCollectionViewModelType) {
        self.viewModel = viewModel
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError(.wrongInit)
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel.bindActivityIndicator()
        self.viewModel.bindErrorTracker()
        self.viewModel.inputs.viewDidLoad()

        setupCollectionView()

        defer {
            bindViewModel()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func bindTexts() {
        self.title = "COLLECTION" //RLocalized.title_likes()
    }
    
    override func bindStyles() {
    }
    
    // MARK: - Binding
    private func bindViewModel() {
        // Outputs
        self.viewModel.outputs.usersDriver
            .drive(self.collectionView.rx.items(dataSource: dataSource()))
            .disposed(by: disposeBag)
        
        self.viewModel.outputs.isEmpty
            .bind(to: self.rx.isEmpty)
            .disposed(by: self.disposeBag)
        
        self.viewModel.outputs.isError
            .bind(to: self.rx.isError)
            .disposed(by: self.disposeBag)
        
        // Inputs
        collectionView.rx.itemSelected
            .asDriver()
            .drive(onNext: { [weak self] (indexPath) in
                self?.viewModel.inputs.userSelected(index: indexPath.row)
            })
            .disposed(by: self.disposeBag)
    }
    
    fileprivate func setupCollectionView() {
        collectionView.register(R.nib.userCollectionViewCell)
        collectionView.rx.setDelegate(self)
            .disposed(by: disposeBag)
    }
}

extension Reactive where Base: UserCollectionViewController {
    var isEmpty: AnyObserver<Void> {
        return Binder<Void>(self.base) { controller, isEmpty in
            let emptyVC = EmptyViewController(title: RLocalized.text_list_empty(), action: { vc in
                vc.remove()
                controller.viewModel.inputs.viewDidLoad()
            })
            
            controller.add(emptyVC)
            
            }.asObserver()
    }
}

extension Reactive where Base: UserCollectionViewController {
    var isError: AnyObserver<Void> {
        return Binder<Void>(self.base) { controller, isError in
            let errorVC = ErrorViewController(title: RLocalized.text_list_error(), action: { vc in
                vc.remove()
                controller.viewModel.inputs.viewDidLoad()
            })
            
            controller.add(errorVC)
            
            }.asObserver()
    }
}

// MARK: - RxDataSource
extension UserCollectionViewController {
    fileprivate func dataSource() -> RxCollectionViewSectionedReloadDataSource<UserSection> {
        return RxCollectionViewSectionedReloadDataSource<UserSection>(configureCell: { dataSource, collectionView, indexPath, item in
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.userCollectionViewCell, for: indexPath)!
            cell.configure(with: item)
            return cell
        }
        )
    }
}

extension UserCollectionViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let itemSize = (UIDevice.current.orientation.isLandscape) ? collectionView.bounds.height : collectionView.bounds.width
        return CGSize(width: itemSize*0.44, height: itemSize/1.5)
    }
}

// MARK: Device Orientation
extension UserCollectionViewController {
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        collectionView.reloadData()
    }
}
