//
//  AppEnvironment.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation

public struct AppEnvironment {
    
    fileprivate static var stack: [Environment] = []
    
    static func setup() {
        let env = fromStorage()
        self.stack.append(env)
    }
    
    // The most recent environment on the stack.
    public static var current: Environment! {
        return stack.last
    }
    
    // Push a new environment onto the stack.
    public static func pushEnvironment(_ env: Environment) {
        saveEnvironment(environment: env)
        stack.append(env)
    }
    
    // Saves some key data for the current environment
    internal static func saveEnvironment(environment env: Environment = AppEnvironment.current) {
        Defaults[.baseURL] = env.baseURL
    }
    
    static func fromStorage() -> Environment {
        return Environment(baseURL: Defaults[.baseURL])
    }
    
    static func log() {
        if let current = self.current {
            debugPrint("******************************************** Build Configuration: \(current.buildConfiguration) ********************************************")
        }
    }
}
