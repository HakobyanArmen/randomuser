//
//  APIErrorCode.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation

enum APIErrorCode: String {
    // Generic
    case unknown = "error.unknown"
    case request_invalid = "request.invalid"
    case internal_server = "error.internal_server"
    case unauthorized = "error.unauthorized"
    case forbidden = "error.forbidden"
}

extension APIErrorCode {
    init() {
        self = .unknown
    }

    init?(_ value: String) {
        switch value.lowercased() {
        case "error.unknown": self = .unknown
        case "request.invalid": self = .request_invalid
        case "error.internal_server": self = .internal_server
        case "error.unauthorized": self = .unauthorized
        case "error.forbidden": self = .forbidden
        default: return nil
        }
    }
    
    func toLocalizableString() -> String {
        return self.rawValue.localized()
    }
}
