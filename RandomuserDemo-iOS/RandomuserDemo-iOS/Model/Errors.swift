//
//  Errors.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation

protocol ErrorReasonType {}

enum APIErrorReason: ErrorReasonType {
    case sessionUnavailable
    case invalidURL(url: String)
    case general
    case authenticationRequired
    case verificationRequired
    case unauthenticatedRequest(err: APIError?)
}

enum MappingErrorReason: ErrorReasonType {
    case keyNotFound(key: String)
    case valueNotFound(forKey: String)
}

protocol ErrorType: Swift.Error {
    associatedtype ReasonType: ErrorReasonType
    
    var formattedError: String { get }
    var reason: ReasonType { get }
    
    static func error(withReason reason: ReasonType) -> Self
    init(reason: ReasonType)
}

extension ErrorType {
    var localizedDescription: String {
        return formattedError
    }
    
    static func error(withReason reason: ReasonType) -> Self {
        return self.init(reason: reason)
    }
}

enum APIError: LocalizedError, DecodableModel {
    case general(status: String, code: APIErrorCode, message: String, description: String)
    
    init(from decoder: Decoder) throws {        
        let container = try decoder.singleValueContainer()
        let response = try container.decode(APIErrorResponse.self)
        
        guard let errors = response.errors, errors.count > 0 else {
            throw MappingError(reason: .valueNotFound(forKey: "errors"))
        }

        guard let status = response.status else {
            throw MappingError(reason: .valueNotFound(forKey: "status"))
        }
        
        let codes = errors.map { error -> APIErrorCode in
            guard let code = error.code else {
                return .unknown
            }
            
            return APIErrorCode(rawValue: code) ?? .unknown
        }

        let descriptions = errors.map { error -> String in
            if let description = error.description {
                return description
            }
            
            return "Error description is empty"
        }

        self = .general(status: "\(status)", code: codes.first ?? .unknown,
                        message: "userMessage", description: descriptions.joined(separator: "\n"))
    }
    
    var errorDescription: String? {
        return self.localizedDescription
    }
    
    var localizedDescription: String {
        switch self {
        case let .general(_, code, _, _):
            return code.toLocalizableString()
        }
    }
}

struct APIErrorResponse: DecodableModel {
    let status: Int?
    let code: String?
    let path: String?
    let timestamp: String?
    let errors: [APISingleError]?
    
    private enum CodingKeys: String, CodingKey {
        case status, code, path, timestamp, errors
    }
}

struct APISingleError: DecodableModel {
    let code: String?
    let userMessage: String?
    let description: String?
    
    private enum CodingKeys: String, CodingKey {
        case code, userMessage, description
    }
}

extension APIErrorResponse: Equatable {
    static func ==(lhs: APIErrorResponse, rhs: APIErrorResponse) -> Bool {
        return lhs.status == rhs.status
    }
}

// #dev Update naming after old code cleanup. A.H.
struct ErrorV1: ErrorType, LocalizedError {
    let reason: APIErrorReason
    var formattedError: String {
        switch reason {
        case .authenticationRequired:
            return "Authentication required"
        case .general:
            return "Unknown error"
        case let .invalidURL(url):
            return "Invalid URL: \(url)"
        case .sessionUnavailable:
            return "Session not available"
        case .unauthenticatedRequest:
            return "Unauthenticated request"
        case .verificationRequired:
            return "Verification required"
        }
    }
    
    var errorDescription: String? {
        return self.localizedDescription
    }
    
    var localizedDescription: String {
        switch reason {
        case .authenticationRequired:
            return "Authentication required"
        case .general:
            return "Unknown error"
        case let .invalidURL(url):
            return "Invalid URL: \(url)"
        case .sessionUnavailable:
            return "Session not available"
        case .unauthenticatedRequest(let err):            
            return err?.localizedDescription ?? "Unauthenticated request"
        case .verificationRequired:
            return "Verification required"
        }
    }
}

enum InnerError: LocalizedError {
    case wrongInit
    case noParam
    
    var localizedDescription: String {
        switch self {
        case .wrongInit:
            return "init has not been implemented"
        case .noParam:
            return "parameter does not exist"
        }
    }
    
    var errorDescription: String? {
        return self.localizedDescription
    }
}

struct MappingError: ErrorType {
    let reason: MappingErrorReason
    
    var formattedError: String {
        switch reason {
        case let .keyNotFound(key):
            return "Key not found: \(key)"
        case let .valueNotFound(forKey):
            return "Value not found for key: \(forKey)"
        }
    }
}
