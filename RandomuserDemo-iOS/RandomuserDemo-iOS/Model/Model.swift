//
//  Model.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation

protocol ModelType {}

// let this take control, instead of Mappable protocol
protocol DecodableModel: ModelType, Decodable {}

struct EmptyDecodableModel: DecodableModel {}
