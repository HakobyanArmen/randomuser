//
//  DiscoveryUser.swift
//  RandomuserDemo-iOS
//
//  Created by Armen Hakobyan on 03/03/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation

struct DiscoveryUsers: DecodableModel, Encodable {
    let users: [DiscoveryUser]?
    
    private enum CodingKeys: String, CodingKey {
        case users = "results"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.users = try? container.decode([DiscoveryUser].self, forKey: .users)
    }
}

struct DiscoveryUser: DecodableModel, Encodable {

    let gender: String?
    let email: String?
    let phone: String?
    let cell: String?
    let picture: Picture?
    let location: Location?
    let name: Name?
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.gender = try? container.decode(String.self, forKey: .gender)
        self.email = try? container.decode(String.self, forKey: .email)
        self.phone = try? container.decode(String.self, forKey: .phone)
        self.cell = try? container.decode(String.self, forKey: .cell)
        self.picture = try? container.decode(Picture.self, forKey: .picture)
        self.location = try? container.decode(Location.self, forKey: .location)
        self.name = try? container.decode(Name.self, forKey: .name)
    }
}

struct Name: DecodableModel, Encodable {
    let title: String?
    let first: String?
    let last: String?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.title = try? container.decode(String.self, forKey: .title)
        self.first = try? container.decode(String.self, forKey: .first)
        self.last = try? container.decode(String.self, forKey: .last)
    }
}

struct Location: DecodableModel, Encodable {
    let street: String?
    let city: String?
    let state: String?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.street = try? container.decode(String.self, forKey: .street)
        self.city = try? container.decode(String.self, forKey: .city)
        self.state = try? container.decode(String.self, forKey: .state)
    }
}

struct Picture: DecodableModel, Encodable {
    let large: String?
    let medium: String?
    let thumbnail: String?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.large = try? container.decode(String.self, forKey: .large)
        self.medium = try? container.decode(String.self, forKey: .medium)
        self.thumbnail = try? container.decode(String.self, forKey: .thumbnail)
    }
}
